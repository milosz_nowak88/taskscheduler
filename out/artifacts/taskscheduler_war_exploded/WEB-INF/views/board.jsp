<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <%--<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/style.css"/>--%>
    <link rel='stylesheet' href='<c:url value="/resources/css/style.css" />' type='text/css' media='all' />
    <title>TaskScheduler</title>
</head>

<body>
<h1>TASK BOARD BY MILO & SEBIX</h1>
<br/>
<div>
    <form action="<c:url value='/newTask' />">
        <input type="submit" value="Add New Task" />
    </form>
    <form action="<c:url value='/newUser' />">
        <input type="submit" value="Add New User" />
    </form>
</div>
<div class="container">
    <div class="content">
        <h1>NEW</h1>
    <c:forEach items="${newTasks}" var="task">
        <div class="news">
            <div id="${task.getType()}"><p></p></div>
            <div id="taskcontent">
                <h2><a href="<c:url value='/view-${task.getId()}-task' />">${task.getTitle()}</a></h2>
                <div class="szczegoly">
                    Added by: <a href="#">Admin</a>, on <c:out value="${task.getCreated()}"/>
                </div>
                <div class="wiecej">
                    Priority: ${task.getPriority()} | Severity: ${task.getSeverity()}
                    <br/>
                    <a href="<c:url value='/assign-${task.getId()}-task' />">Assign</a>
                </div>
                <div class="tresc">
                    <img src="images/widok.png" alt="" height="30" width="30">
                    <c:out value="${task.getContent()}"/>
                </div>
            </div>
        </div>
    </c:forEach>
    </div>
    <div class="content">
    <h1>ACTIVE</h1>
    <c:forEach items="${activeTasks}" var="task">
        <div class="news">
            <div id="${task.getType()}"><p></p></div>
            <div id="taskcontent">
                <h2><a href="<c:url value='/view-${task.getId()}-task' />">${task.getTitle()}</a></h2>
                <div class="szczegoly">
                    Added by: <a href="#">Admin</a>, on <c:out value="${task.getCreated()}"/>
                </div>
                <div class="wiecej">
                    Priority: ${task.getPriority()} | Severity: ${task.getSeverity()}
                    <br/>
                    Assigned to: <a href="<c:url value='/assign-${task.getId()}-task' />"><c:out value="${task.getUser().getName()}"/></a>
                </div>
                <div class="tresc">
                    <img src="<c:out value='${task.getUser().getAvatar_url()}'/>" alt="" height="30" width="30">
                    <c:out value="${task.getContent()}"/>
                </div>
            </div>
        </div>
    </c:forEach>
</div>
    <div class="content">
        <h1>RESOLVED</h1>
        <c:forEach items="${resolvedTasks}" var="task">
            <div class="news">
                <div id="${task.getType()}"><p></p></div>
                <div id="taskcontent">
                    <h2><a href="<c:url value='/view-${task.getId()}-task' />">${task.getTitle()}</a></h2>
                    <div class="szczegoly">
                        Added by: <a href="#">Admin</a>, on <c:out value="${task.getCreated()}"/>
                    </div>
                    <div class="wiecej">
                        Priority: ${task.getPriority()} | Severity: ${task.getSeverity()}
                        <br/>
                        Assigned to: <a href="<c:url value='/assign-${task.getId()}-task' />"><c:out value="${task.getUser().getName()}"/></a>
                    </div>
                    <div class="tresc">
                        <img src="images/widok.png" alt="" height="30" width="30">
                        <c:out value="${task.getContent()}"/>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
    <div class="content">
        <h1>CLOSED</h1>
        <c:forEach items="${closedTasks}" var="task">
            <div class="news">
                <div id="${task.getType()}"><p></p></div>
                <div id="taskcontent">
                    <h2><a href="<c:url value='/view-${task.getId()}-task' />">${task.getTitle()}</a></h2>
                    <div class="szczegoly">
                        Added by: <a href="#">Admin</a>, on <c:out value="${task.getCreated()}"/>
                    </div>
                    <div class="wiecej">
                        Priority: ${task.getPriority()} | Severity: ${task.getSeverity()}
                        <br/>
                        Assigned to: <a href="<c:url value='/assign-${task.getId()}-task' />"><c:out value="${task.getUser().getName()}"/></a>
                    </div>
                    <div class="tresc">
                        <img src="images/widok.png" alt="" height="30" width="30">
                        <c:out value="${task.getContent()}"/>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>

</body>
</html>
