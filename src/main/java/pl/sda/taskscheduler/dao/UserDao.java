package pl.sda.taskscheduler.dao;

import pl.sda.taskscheduler.model.User;

import java.util.List;

/**
 * Created by Koputer on 2016-12-19.
 */
public interface UserDao {

    User findById(long id);

    void saveUser(User user);

    void deleteUserbyId(long id);

    List<User> findByUserName(String name);

    List<User> findByUserSurname(String surname);

    List<User> findByUserNameAndSurname(String name,String surname);
}
